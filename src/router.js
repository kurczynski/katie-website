import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Research from './views/Research'
import Talks from './views/Talks'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/research/:pubPath',
      name: 'research',
      component: Research,
      props: true
    },
    {
      path: '/talks',
      name: 'talks',
      component: Talks
    }
  ],
  mode: 'history'
})
