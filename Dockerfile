FROM node AS builder
WORKDIR /usr/local/build
COPY css/ css/
COPY src/ src/
COPY public/ public/
COPY package.json ./
COPY .eslintrc ./
RUN npm install
RUN npm run build

FROM nginx:alpine AS app
COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /usr/local/build/dist /katie-website/
